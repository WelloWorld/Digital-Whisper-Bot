#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
from time import sleep

base_url = 'https://www.digitalwhisper.co.il'
NUM_OF_MAGAZINES = 91

def pdf_filter(i):
	return ('DW' in i and 'pdf' in i)

for j in range(1,NUM_OF_MAGAZINES+1):

	html_doc = requests.get(base_url+'/issue2').text
	soup = BeautifulSoup(html_doc,'html.parser')

	for i in soup.find_all('a'):
		if pdf_filter(str(i)):
			if '..' in str(i['href']): #For relative urls like ../../files/..........
				d = base_url + i['href'][i['href'].find('/files'):]
			else:
				d = i['href']	
			
			pdf = requests.get(d)
			print i.contents[0] #Name of magazine
			with open('['+str(j) +'] - '+i.contents[0] + '.pdf','w') as f:
				f.write(pdf.content)	
	sleep(2)			